package gr.ilsp.nlpli.keycloak;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.http.impl.client.CloseableHttpClient;
import org.jboss.logging.Logger;
import org.keycloak.Config;
import org.keycloak.connections.httpclient.HttpClientBuilder;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

/**
 * 
 * ELG keycloak event listener.
 * 
 * @author galanisd
 *
 */
public class KeycloakEventListenerFactory implements EventListenerProviderFactory {

	private static final Logger logger = Logger.getLogger(KeycloakEventListenerFactory.class.getName());
	private CloseableHttpClient client = new HttpClientBuilder().build();

	private Logger.Level successLevel;
	private Logger.Level errorLevel;

	private ExecutorService executorService = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,
			new LinkedBlockingQueue<Runnable>());

	private boolean firstRun = false;	

	private ConfigProperties configProps;
	
	@Override
	public EventListenerProvider create(KeycloakSession keycloakSession) {
		KeycloakEventListener listener = new KeycloakEventListener(keycloakSession, logger, successLevel, errorLevel,
				executorService, client, firstRun, configProps);
		if (firstRun)
			firstRun = false;
		return listener;
	}

	@Override
	public void init(Config.Scope scope) {
		configProps = new ConfigProperties();
		configProps.loadEnvProperties();
	}

	@Override
	public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

	}

	@Override
	public void close() {

	}

	@Override
	public String getId() {
		return "ELG_catalogue_notifier";
	}

}
