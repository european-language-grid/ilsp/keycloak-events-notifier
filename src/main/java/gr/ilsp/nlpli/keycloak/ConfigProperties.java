package gr.ilsp.nlpli.keycloak;

import java.util.HashSet;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

/**
 * 
 * Config properties
 * 
 * @author galanisd
 *
 */
public class ConfigProperties {
	
	private static final Logger logger = Logger.getLogger(ConfigProperties.class.getName());
		
	
	/** CONFIG */
	private final static String CONFIG = "CONFIG";
	/** API key for communicating with catalogue property */
	private final static String CATALOGUE_API_KEY = "CATALOGUE_API_KEY";
	/** ROLE CHANGE IS SENT */
	private final static String REALM_ROLE_IS_SENT = "REALM_ROLE_IS_SENT";
	/** ROLE CHANGE IS SENT */
	private final static String REALM_USED = "REALM_USED";
	/** CATALOGUE_ENDPOINT */
	private final static String CATALOGUE_ENDPOINT = "CATALOGUE_ENDPOINT";
	
	private String config;
	private String catalogueAPIKey;	
	private String realm; 	
	private String catalogueEndpoint;
	private boolean roleMapping = true;
	private HashSet<String> catalogueAppRoles = new HashSet<String>();
	
	public final static String ELG = "ELG";
	public final static String CLARIN = "CLARIN";
	
	public ConfigProperties() {
		
	}
	
	public void loadEnvProperties() {
		logger.log(Level.INFO, "load ENV properties");
		
		catalogueAPIKey = System.getenv(CATALOGUE_API_KEY);
		System.out.println("catalogueAPIKey:" + catalogueAPIKey);
		
		if (catalogueAPIKey != null) {
			catalogueEndpoint = System.getenv(CATALOGUE_ENDPOINT) +"?token=" + catalogueAPIKey;
			logger.log(Level.INFO, "catalogueEndpoint:" + catalogueEndpoint);
		} else {
			logger.log(Level.INFO, "No CATALOGUE_API_KEY provided..");
		}
		
		if(System.getenv(REALM_ROLE_IS_SENT) != null) {
			roleMapping = Boolean.parseBoolean(System.getenv(REALM_ROLE_IS_SENT));
		} else {
			roleMapping = true;
		}
		
		if(System.getenv(CONFIG) != null) {
			config = System.getenv(CONFIG);
			
			if(config.equalsIgnoreCase(ELG)) {
				loadELG();
			} else if (config.equalsIgnoreCase(CLARIN)) {
				
			}
		}
		
		if(System.getenv(REALM_USED) != null) {
			realm = System.getenv(REALM_USED);
		}				
	}

	
	public String getCatalogueAPIKey() {
		return catalogueAPIKey;
	}

	public void setCatalogueAPIKey(String CatalogueAPIKEY) {
		this.catalogueAPIKey = CatalogueAPIKEY;
	}

	public boolean isRoleMappingEnabled() {
		return roleMapping;
	}

	public void setRoleMappingEnabled(boolean roleMapping) {
		this.roleMapping = roleMapping;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}
	

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	
	public HashSet<String> getCatalogueAppRoles() {
		return catalogueAppRoles;
	}

	public void setCatalogueAppRoles(HashSet<String> catalogueAppRoles) {
		this.catalogueAppRoles = catalogueAppRoles;
	}
	
	public String getCatalogueEndpoint() {
		return catalogueEndpoint;
	}

	public void setCatalogueEndpoint(String catalogueEndpoint) {
		this.catalogueEndpoint = catalogueEndpoint;
	}

	private void loadELG (){
		// list of roles
		catalogueAppRoles.add("admin");
		catalogueAppRoles.add("consumer");
		catalogueAppRoles.add("provider");
		catalogueAppRoles.add("legal_validator");
		catalogueAppRoles.add("technical_validator");
		catalogueAppRoles.add("metadata_validator");
		catalogueAppRoles.add("content_manager");
		catalogueAppRoles.add("elg_tester");
		catalogueAppRoles.add("flat_rate_consumer");
		
		// at some point the following role was created that includes
		// all the default assigned.
		catalogueAppRoles.add("default-roles-elg");
		
	}
}
