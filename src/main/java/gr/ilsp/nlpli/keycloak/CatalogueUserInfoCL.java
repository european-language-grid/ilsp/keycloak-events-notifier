package gr.ilsp.nlpli.keycloak;

public class CatalogueUserInfoCL extends CatalogueUserInfoBase{

	private String affiliation;
	private String position;
	private String website;
	private String originalIDP;
	
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {		
		this.affiliation = affiliation;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getOriginalIDP() {
		return originalIDP;
	}
	public void setOriginalIDP(String originalIDP) {
		this.originalIDP = originalIDP;
	}		

}
