package gr.ilsp.nlpli.keycloak;

import java.util.ArrayList;

public class CatalogueUserInfoBase{

	private String operation;
	private String username;
	private String userID;
	private String keycloak_id; 
	private String first_name; 
	private String last_name; 
	private String email;
	private boolean enabled; 
	private ArrayList<String> roles;
	
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getKeycloak_id() {
		return keycloak_id;
	}
	public void setKeycloak_id(String keycloak_id) {
		this.keycloak_id = keycloak_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public ArrayList<String> getRoles() {
		return roles;
	}
	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public static String nullToEmpty(String value) {
		if(value == null) {
			return "";
		} else {
			return value;
		}
	}
}
