package gr.ilsp.nlpli.keycloak;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.events.admin.OperationType;
import org.keycloak.events.admin.ResourceType;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.UserProvider;
import org.keycloak.util.JsonSerialization;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * 
 * A listener that notifies ELG catalogue backend for changes in Keycloak; e.g.
 * update profile, role changes.   
 * 
 * @author galanisd
 *
 */
public class KeycloakEventListener implements EventListenerProvider {
		
	private CloseableHttpClient client; //= new HttpClientBuilder().build();
	
	private KeycloakSession session;	
	private ExecutorService executorService;
	private final Logger logger;
	private final Logger.Level successLevel;
	private final Logger.Level errorLevel;
	
	private ConfigProperties configProps;
	
	public KeycloakEventListener(KeycloakSession session, Logger logger, Logger.Level successLevel,
			Logger.Level errorLevel, ExecutorService executorService, CloseableHttpClient client, boolean first, ConfigProperties configProps) {

		this.session = session;
		this.logger = logger;
		this.successLevel = successLevel;
		this.errorLevel = errorLevel;		
		this.executorService = executorService;
		this.client = client;
		this.configProps = configProps;
		
		//logger.info(catalogueEndpoint);
		if(first)
			syncAll();
	}

	@Override
	public void close() {
		
	}

	private void syncAll() {
		RealmModel realm = session.realms().getRealm(configProps.getRealm());
		List<UserModel> allUsers = session.users().getUsers(realm);
		for(int i = 0; i < allUsers.size(); i++) {
			logger.info("user " + allUsers.get(i).getId());
			
			CatalogueUserInfoBase updatedUserInfo = extractCatalogueUserInfo(allUsers.get(i));		
			updatedUserInfo.setUserID(allUsers.get(i).getId());
			updatedUserInfo.setOperation("CREATE_PROFILE");
			//syncToCatalogue(updatedUserInfo);
		}
	}
	
	@Override
	public void onEvent(Event event) {		

		if (event.getType().equals(EventType.VERIFY_EMAIL)) {
			// This is for the case where a new user is registered. 
			// Registration is confirmed via a verification email. 
			// We do not use REGISTER event because 
			// http://keycloak-user.88327.x6.nabble.com/keycloak-user-Firstname-and-Lastname-are-null-when-REGISTER-event-td490.html
			// "I have a listener for REGISTER event and it works well, but when I try to get the first and last names of the user are null, 
			// but all other fields are filled as mail, telephone, etc."
			logger.log(Level.INFO, "** Event Occurred ** " + event.getType());
			// Register.
			handle(event);
		}  else if (event.getType().equals(EventType.UPDATE_PROFILE)) {
			// A new user updates its profile. This is done via the catalogue UI.
			logger.log(Level.INFO, "** Event Occurred ** " + event.getType());
			// Profile update for user.
			handle(event);
		} else if (event.getType().equals(EventType.LOGIN)) {
			// A new user logins. This is done via the catalogue UI.
			logger.log(Level.INFO, "** Event Occurred ** " + event.getType());
			// Profile update for user.
			handle(event);			
		}
	}


	@Override
	public void onEvent(AdminEvent adminEvent, boolean includeRepresentation) {		
		// Role change for user.
		if (adminEvent.getResourceType().equals(ResourceType.REALM_ROLE_MAPPING) && configProps.isRoleMappingEnabled()) {
			// A keycloak admin changes a role for a user 
			logger.log(Level.INFO, "** Admin Event Occurred ** ");
			handleAdminEvent(adminEvent, ResourceType.REALM_ROLE_MAPPING.toString());
		} else if (adminEvent.getResourceType().equals(ResourceType.USER)) {
			// A keycloak admin ... 
			logger.log(Level.INFO, "** Admin Event Occurred ** ");
			if (adminEvent.getOperationType().equals(OperationType.CREATE) ) {
				// creates a new user
				handleAdminEvent(adminEvent, "CREATE_PROFILE");	
			} else if(adminEvent.getOperationType().equals(OperationType.UPDATE) ) {
				// updated a user profile
				handleAdminEvent(adminEvent, EventType.UPDATE_PROFILE.toString());				
			}
		}
	}

	// -- -- --- -- -- -- -- --- -- -- -- -- --- -- -- -- -- --- -- --
	// -- -- --- -- -- -- -- --- -- -- -- -- --- -- -- -- -- --- -- --
	
	/**
	 * Handle an event
	 * 
	 * @param event
	 */
	private void handle(Event event) {
		UserModel um = findKeycloakUserInfoBasedOnEventInfo(event);
		CatalogueUserInfoBase updatedUserInfo = extractCatalogueUserInfo(um);		
		updatedUserInfo.setUserID(event.getUserId());
		updatedUserInfo.setOperation(event.getType().toString());
		syncToCatalogue(updatedUserInfo);
	}

	
	private void handleAdminEvent(AdminEvent adminEvent, String operation) {
		// Extract userID from ResourcePath
		String userID = getUserIDFromResourcePath(adminEvent);
		UserModel um = findKeycloakUserInfoBasedOnAdminEventInfo(adminEvent, userID);
		CatalogueUserInfoBase updatedUserInfo = extractCatalogueUserInfo(um);
		updatedUserInfo.setUserID(userID);
		updatedUserInfo.setOperation(operation);
		syncToCatalogue(updatedUserInfo);		
	}
	
	private String getUserIDFromResourcePath(AdminEvent adminEvent) {
		int start = adminEvent.getResourcePath().indexOf("/");
		int end = adminEvent.getResourcePath().indexOf("/", start + 1);		
		String userID = "";
		if(end != -1) {
			userID = adminEvent.getResourcePath().substring(start + 1, end);
		} else {
			userID = adminEvent.getResourcePath().substring(start + 1);
		}		
		logger.log(Level.INFO, "getUserIDFromResourcePath" + userID);
		
		return userID;
	}
	
	private UserModel findKeycloakUserInfoBasedOnEventInfo(Event event) {
		RealmModel realm = session.realms().getRealm(event.getRealmId());				
		UserProvider userProvider = session.getProvider(UserProvider.class);
		UserModel um = userProvider.getUserById(event.getUserId(), realm);		  
		//UserModel um = session.users().getUserById(event.getUserId(), realm);		
		
		return um;
	}

	private UserModel findKeycloakUserInfoBasedOnAdminEventInfo(AdminEvent event, String userID) {
		RealmModel realm = session.realms().getRealm(configProps.getRealm());
		UserModel um = session.users().getUserById(userID, realm);		
		return um;
	}

	/**
	 * 
	 * Get from Keycloak the required user's info and put them in a
	 * {@link CatalogueUserInfoBase} object
	 * 
	 * @param userModel user model ({@link UserModel}) from Keycloak
	 * @return a {@link CatalogueUserInfoBase} object
	 */
	private <T extends CatalogueUserInfoBase> T extractCatalogueUserInfo(UserModel userModel) {
		// Log
		logger.log(Level.INFO, "last:" + userModel.getLastName());
		logger.log(Level.INFO, "first:" + userModel.getFirstName());
		logger.log(Level.INFO, "email:" + userModel.getEmail());

		if (this.configProps.getConfig().equalsIgnoreCase(ConfigProperties.ELG)) {
			// Build CatalogueUserInfo.
			CatalogueUserInfoELG user = new CatalogueUserInfoELG();
			user.setLast_name(userModel.getLastName());
			user.setFirst_name(userModel.getFirstName());
			user.setEmail(userModel.getEmail());
			user.setEnabled(userModel.isEnabled());
			user.setUsername(userModel.getUsername());			
			
			ArrayList<String> effectiveRolesList = getEffectiveRoles(userModel);				
			user.setRoles(effectiveRolesList);
			
			return (T)user;
		} else if (this.configProps.getConfig().equalsIgnoreCase(ConfigProperties.CLARIN)) {
			// Build CatalogueUserInfo.
			CatalogueUserInfoCL user = new CatalogueUserInfoCL();
			user.setLast_name(userModel.getLastName());
			user.setFirst_name(userModel.getFirstName());
			user.setEmail(userModel.getEmail());
			user.setEnabled(userModel.isEnabled());
			user.setUsername(userModel.getUsername());			
			
			user.setAffiliation(CatalogueUserInfoCL.nullToEmpty(userModel.getFirstAttribute("Affiliation")));
			user.setWebsite(CatalogueUserInfoCL.nullToEmpty(userModel.getFirstAttribute("Website")));
			user.setPosition(CatalogueUserInfoCL.nullToEmpty(userModel.getFirstAttribute("Position")));
			user.setOriginalIDP(CatalogueUserInfoCL.nullToEmpty(userModel.getFirstAttribute("OriginalIDP")));			
			
			user.setRoles(new ArrayList<String>());		
			logger.log(Level.INFO, "Affiliation:" + user.getAffiliation());
			logger.log(Level.INFO, "Website:" + user.getWebsite());
			logger.log(Level.INFO, "Position:" + user.getPosition());			
			logger.log(Level.INFO, "OriginalIDP:" + user.getOriginalIDP());
			
			return (T)user;
		}
		
		return null;
	}

	/**
	 * Get effective roles for user. We are interested only in Catalogue application roles.
	 *  
	 * @param userModel
	 * @return the list of effective roles
	 */
	private ArrayList<String> getEffectiveRoles(UserModel userModel) {
		// A set that contains the effective roles of a user
		HashSet<String> effectiveRolesSet = new HashSet<String>();
		Set<RoleModel> roles = userModel.getRoleMappings();
		searchForEffectiveRoles(roles, effectiveRolesSet);		
		
		return new ArrayList<String>(effectiveRolesSet) ;
	}
	
	/**
	 * Recursively search and find the effective roles.
	 * Start from {@code rolesToSearch} set. 
	 * 
	 * @param rolesToSearch
	 * @param effectiveRolesSet
	 */
	private void searchForEffectiveRoles(Set<RoleModel> rolesToSearch, HashSet<String> effectiveRolesSet) {
		rolesToSearch.forEach(role -> {
			if (isCatalogueAppRole(role.getName())) {  
				logger.log(Level.INFO, role.getName());
				// If a role is composite and is not already contained in effectiveRolesSet
				// then explore it.
				if(role.isComposite() && !effectiveRolesSet.contains(role.getName())) {
					Set<RoleModel> composites = role.getComposites();
					searchForEffectiveRoles(composites, effectiveRolesSet);
				}
				
				effectiveRolesSet.add(role.getName());
			}
		});
	}
	
	private boolean isCatalogueAppRole(String role) {
		if (configProps.getCatalogueAppRoles().contains(role)) {
			return true;
		} else {
			return false;
		}
	}

	private String toString(AdminEvent event) {
		return event.toString();
	}

	/**
	 * Sync user's info to catalogue by sending a {@link CatalogueUserInfoBase} to the
	 * configured catalogue endpoint via POST.
	 * 
	 * @param user as  object
	 */
	private void syncToCatalogue(Object user) {
		String message = "";

		try {
			message = JsonSerialization.mapper.writeValueAsString(user);
			
			HttpPost httpPost = new HttpPost(configProps.getCatalogueEndpoint());
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");			
			httpPost.setEntity(new StringEntity(message, Charset.forName("UTF-8")));

			Runnable runnableTask = () -> {
				CloseableHttpResponse response = null;
				try {
					response = client.execute(httpPost);
					logger.log(Level.INFO, "Sent");					
					// TimeUnit.MILLISECONDS.sleep(300);
				} catch (ClientProtocolException e) {
					logger.log(Level.INFO, e);
				} catch (IOException e) {
					logger.log(Level.INFO, e);					
				} finally {
					try {
						response.close();
					} catch (IOException e) {
						logger.log(Level.INFO, e);
					}
				}
			};

			executorService.execute(runnableTask);
			logger.log(Level.INFO, "Queued - > " + message);

		} catch (JsonProcessingException e) {
			logger.log(Level.INFO, e);
		//} catch (UnsupportedEncodingException e) {
		//	logger.log(Level.INFO, e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

