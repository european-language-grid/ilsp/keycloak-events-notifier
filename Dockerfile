# Base image.
FROM openjdk:8-jdk-alpine

# COPY notifier jar to /elg
ENV TARGETDIR /elg/
RUN mkdir -p $TARGETDIR
ADD /target/sample-event-listener.jar ${TARGETDIR}/elg-notifier.jar

# Set working dir. 
WORKDIR ${TARGETDIR}
